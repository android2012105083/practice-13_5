package example.ndkcaculator2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {



    static {
        System.loadLibrary("calculate");
    }
    public native float add(float x, float y);
    public native float mul(float x, float y);
    public native float div(float x, float y);
    public native float min(float x, float y);

    TextView display;
    String operator = "";
    String num1 = "";
    String num2 = "";
    String modi ="";

    TextView t1;
    TextView t2;
    TextView t3;
    TextView t4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
      //  display = (TextView) findViewById(R.id.display);
    }

    public void onClick(View v) {
        if(operator == ""){
        switch (v.getId()) {
            case R.id.one:
                num1 = num1+"1";
                break;
            case R.id.two:
                num1 = num1+"2";
                break;
            case R.id.three:
                num1 = num1+"3";
                break;
            case R.id.four:
                num1 = num1+"4";
                break;
            case R.id.five:
                num1 = num1+"5";
                break;
            case R.id.six:
                num1 = num1+"6";
                break;
            case R.id.seven:
                num1 = num1+"7";
                break;
            case R.id.eight:
                num1 = num1+"8";
                break;
            case R.id.nine:
                num1 = num1+"9";
                break;
            case R.id.zero:
                num1 = num1+"0";
                break;
            case R.id.point:
                num1 = num1+".";
                break;
            case R.id.mul:
                operator = "*";
                t1 = (TextView)findViewById(R.id.textView);
                t2 = (TextView)findViewById(R.id.textView2);
                t1.setText(num1);
                t2.setText("*");

                break;
            case R.id.add:
                operator = "+";
                t1 = (TextView)findViewById(R.id.textView);
                t2 = (TextView)findViewById(R.id.textView2);
                t1.setText(num1);
                t2.setText("+");
                break;
            case R.id.divide:
                operator = "/";
                t1 = (TextView)findViewById(R.id.textView);
                t2 = (TextView)findViewById(R.id.textView2);
                t1.setText(num1);
                t2.setText("/");
                break;
            case R.id.minus:
                operator = "-";
                t1 = (TextView)findViewById(R.id.textView);
                t2 = (TextView)findViewById(R.id.textView2);
                t1.setText(num1);
                t2.setText("-");
                break;
            case R.id.clear:
                num1 = "";
                operator = "";
        }
        }
        else{
            switch (v.getId()) {
                case R.id.one:
                    num2 = num2+"1";
                    break;
                case R.id.two:
                    num2 = num2+"2";
                    break;
                case R.id.three:
                    num2 = num2+"3";
                    break;
                case R.id.four:
                    num2 = num2+"4";
                    break;
                case R.id.five:
                    num2 = num2+"5";
                    break;
                case R.id.six:
                    num2 = num2+"6";
                    break;
                case R.id.seven:
                    num2 = num2+"7";
                    break;
                case R.id.eight:
                    num2 = num2+"8";
                    break;
                case R.id.nine:
                    num2 = num2+"9";
                    break;
                case R.id.zero:
                    num2 = num2+"0";
                    break;
                case R.id.point:
                    num2 = num2+".";
                    break;
                case R.id.mul:
                    operator = "*";
                    break;
                case R.id.add:
                    operator = "+";
                    break;
                case R.id.divide:
                    operator = "/";
                    break;
                case R.id.minus:
                    operator = "-";
                    break;
                case R.id.equal:
                    TextView result = (TextView) findViewById(R.id.Result);
                    Log.i("dsfssdf",num1 + "  ,"+num2);
                    t3 = (TextView)findViewById(R.id.textView3);
                    t3.setText(num2);

                    float v1, v2, res = -1;
                    try {
                        v1 = Float.parseFloat(num1);
                        v2 = Float.parseFloat(num2);
                    } catch (NumberFormatException e) {
                        v1 = 0; v2 = 0; }
                    if (operator.equals("+")) { res = add(v1, v2); }
                    else if (operator.equals("*")) { res = mul(v1, v2); }
                    else if (operator.equals("/")) { res = div(v1, v2); }
                    else if (operator.equals("-")) { res = min(v1, v2); }
                    result.setText(new Float(res).toString());
                    break;
                case R.id.clear:
                    num1 = "";
                    num2 = "";
                    operator = "";

            }



        }
    }
}
